FROM ubuntu
RUN apt-get -q -y update
RUN apt-get -q -y install build-essential git-core
RUN apt-get -q -y install software-properties-common # To install add-apt-repository tool
RUN add-apt-repository ppa:kurento/kurento
RUN add-apt-repository ppa:mc3man/trusty-media
RUN apt-get -q -y update
RUN apt-get -q -y install kurento-server
RUN apt-get -q -y install x264 libav ffmpeg 

RUN curl -sL https://deb.nodesource.com/setup | sudo bash -

RUN apt-get -q -y install nodejs

RUN mkdir /var/www

ADD app.js /var/www/app.js
ADD package.json /var/www/package.json
ADD environment /etc/environment
ADD static/ /var/www/static/
ADD start.sh /var/www/start.sh
ADD ffserver.conf /var/www/ffserver.conf
ADD feed.sh /var/www/feed.sh

ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
RUN locale-gen en_US.UTF-8

RUN sudo ln /dev/null /dev/raw1394

RUN npm install -g bower
RUN cd /var/www && npm install
RUN cd /var/www/static && bower install --allow-root

CMD ["/bin/bash", "/var/www/start.sh"] 
